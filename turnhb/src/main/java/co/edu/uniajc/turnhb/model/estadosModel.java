package co.edu.uniajc.turnhb.model;

//import lombok.Data;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Estados")
public class estadosModel implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id")
    private long id;
    
    @Column(name="estado")
    private String estado;   

public estadosModel() {
    	//Constructor
}
    
public estadosModel(long id, String estado) {
    this.id = id;
    this.estado = estado;
}

public long getId() { return id; }
public void setId(long id) { this.id = id; }

public String getEstado() { return estado; }
public void setNombre(String estado) { this.estado = estado; }

}

