package co.edu.uniajc.turnhb.controller;

import co.edu.uniajc.turnhb.model.estadosModel;
import co.edu.uniajc.turnhb.service.EstadoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import io.swagger.annotations.*;

@RestController
@RequestMapping("/states")
@Api("States")

public class EstadosController {
	
	 private EstadoService estadoService;

	    @Autowired
	    public EstadosController(EstadoService estadoService){ this.estadoService = estadoService; }
	    
	    @PostMapping(path = "/save")
	    @ApiOperation(value="Insert State",response = estadosModel.class)
	    @ApiResponses(value = {
	            @ApiResponse(code = 400, message = "Algo ha fallado"),
	            @ApiResponse(code = 500, message = "Error interno del servidor")
	    })
	    public estadosModel saveUser(@RequestBody estadosModel estadosModel){
	        return estadoService.save(estadosModel);
	    }

	    public estadosModel getById(int id) {
	        return estadoService.getById(id);
	    }
	    @GetMapping(path = "/all")
	    @ApiOperation(value = "Buscar todos los estados", response = estadosModel.class)
	    public List<estadosModel> findAll() {
	        return estadoService.findAll();
	    }

	    public Optional<estadosModel> findById(Long aLong) {
	        return estadoService.findById(aLong);
	    }
	    @DeleteMapping(path = "/delete")
	    @ApiOperation(value = "Borrar Estado por ID", response = estadosModel.class)
	    public void deleteById(Long aLong) {
	        estadoService.deleteById(aLong);
	    }
	    @PutMapping(path = "/update")
	    @ApiOperation(value = "Update Estado", response = estadosModel.class)
	    public estadosModel update(estadosModel estadosModel) {
	        return estadoService.update(estadosModel);
	    }

}
