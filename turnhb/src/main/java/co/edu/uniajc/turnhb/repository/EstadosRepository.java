package co.edu.uniajc.turnhb.repository;

import co.edu.uniajc.turnhb.model.estadosModel;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface EstadosRepository extends JpaRepository<estadosModel, Long>{
    List<estadosModel> findAllByNamesContains(String estado);
    estadosModel getById(int id);
    @Query(nativeQuery = true, value="SELECT"+
    "id"+",estado" + "FROM Estado"+" WHERE id =: id")
    List<estadosModel> findEstado(@Param(value = "id") Integer id);
}


