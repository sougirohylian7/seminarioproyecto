package co.edu.uniajc.turnhb.controller;

import co.edu.uniajc.turnhb.model.rolModel;
import co.edu.uniajc.turnhb.service.RolService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import io.swagger.annotations.*;

@RestController
@RequestMapping("/rols")
@Api("Rols")
public class RolController {
    private RolService rolService;

    @Autowired
    public RolController(RolService rolService){ this.rolService = rolService; }
    
    @PostMapping(path = "/save")
    @ApiOperation(value="Insert Role",response = rolModel.class)
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Algo ha fallado"),
            @ApiResponse(code = 500, message = "Error interno del servidor")
    })
    public rolModel saveUser(@RequestBody rolModel rolModel){
        return rolService.save(rolModel);
    }

    public rolModel getById(int id) {
        return rolService.getById(id);
    }
    @GetMapping(path = "/all")
    @ApiOperation(value = "Buscar todos los roles", response = rolModel.class)
    public List<rolModel> findAll() {
        return rolService.findAll();
    }

    public Optional<rolModel> findById(Long aLong) {
        return rolService.findById(aLong);
    }
    @DeleteMapping(path = "/delete")
    @ApiOperation(value = "Borrar Rol por ID", response = rolModel.class)
    public void deleteById(Long aLong) {
        rolService.deleteById(aLong);
    }
    @PutMapping(path = "/update")
    @ApiOperation(value = "Update Rol", response = rolModel.class)
    public rolModel update(rolModel rolModel) {
        return rolService.update(rolModel);
    }
}




